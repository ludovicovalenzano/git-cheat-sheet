﻿# GIT CHEAT SHEET
### Basics

> ***git init***
>  
*create a new repo inside the directory*
>***git clone***

*clone the git repository*
>***git config user.name < name>***

*define author name*
>***git add*** 

*stage all changes in < directory> for the next commit*
>***git commit -m "< message>"***

*commit the changes and add a message to the commit*
>***git status***

*list the which files are unstaged and wich are untracked or staged*

>***git log***

*display the entire commit history*

>***git diff***

*show unstaged changes between working directory*

### Undoing changes
>***git revert < commit>***

*create new commit that undoes all of the changes made in the **< commit>  tag**

>***git reset < file>***
 
 *remove file from the staging area but leaves the working directory unchanged*

>***git clean -n***

*Shows which files would be removed from working directory*

### Rewriting GIT history

>***git commit --amend***

*replace the last commit wit the staged changes*

>***git rebase < base>***

*rebase the current branch onto < base>*

>***git reflog*** 

*Show a log changes to the local repository's HEAD*

### Git Branches 
>***git branch***
>
*List all of the branches in your repo*
>***git checkout -b < branch>***

*Create and check out a new branch named "< branch>"
>***git merge < branch>***

*Merge < branch> into the current branch*

### Tracking changes
>***git rm [file]***

*delete the file from project and stage the removal for commit*
>***git mv [existing-path] [new-path]***

*change an existing file path and stage the move*

### Temporary commits
>***git stash***

*save modified and staged changes*

### Share and update
>***git push [alias] [branch]***

*transmit the local commits to the ==remote== repo's branch*


 >***git pull***

*fetch and merge any commits from the remote branch*

#### ==To see more of the commands visit:==

>https://education.github.com/git-cheat-sheet-education.pdf
